package com.bluespurs.productfinderservice;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bluespurs.productfinderservice.bo.Response;
import com.bluespurs.productfinderservice.exception.ProductNotFoundException;
import com.bluespurs.productfinderservice.transformer.ProductFinderTransformer;
import com.bluespurs.productfinderservice.vo.BestBuyData;
import com.bluespurs.productfinderservice.vo.WalmartData;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductFinderApplicationTests {
	ProductFinderTransformer transformer = new ProductFinderTransformer();
	List<WalmartData> walmartList = new ArrayList<WalmartData>();
	WalmartData walmartData1 = new WalmartData();
	List<BestBuyData> bestBuyList = new ArrayList<BestBuyData>();
	BestBuyData bestBuyData1 = new BestBuyData();
	BestBuyData bestBuyData2 = new BestBuyData();
	WalmartData walmartData2 = new WalmartData();
	Response response = null;
	
	@Test
	public void singleRecordObjectComparison() throws ProductNotFoundException {

		walmartData1.setItemId("1");
		walmartData1.setAvailableOnline(true);
		walmartData1.setCurrency("USD");
		walmartData1.setName("Ipad Mini");
		walmartData1.setSalePrice(500);
		walmartList.add(walmartData1);

		bestBuyData1.setProductId("123");
		bestBuyData1.setCurrency("USD");
		bestBuyData1.setName("Ipad Mini");
		bestBuyData1.setSalePrice(510.00);
		bestBuyList.add(bestBuyData1);

		// Compare objects with one record each
		response = transformer.compareObjects(walmartList, bestBuyList);
		assertThat(response.getBestPrice()).isEqualTo(500.00);

	}
	
	@Test
	public void multipleRecordsObjectComparison() throws ProductNotFoundException {
		
		walmartData1.setItemId("1");
		walmartData1.setAvailableOnline(true);
		walmartData1.setCurrency("USD");
		walmartData1.setName("Ipad Mini");
		walmartData1.setSalePrice(500);
		walmartList.add(walmartData1);

		bestBuyData1.setProductId("123");
		bestBuyData1.setCurrency("USD");
		bestBuyData1.setName("Ipad Mini");
		bestBuyData1.setSalePrice(510.00);
		bestBuyList.add(bestBuyData1);
		
		walmartData2.setItemId("2");
		walmartData2.setAvailableOnline(true);
		walmartData2.setCurrency("USD");
		walmartData2.setName("Ipad Mini charger");
		walmartData2.setSalePrice(30.00);
		walmartList.add(walmartData2);

		bestBuyData2.setProductId("456");
		bestBuyData2.setCurrency("USD");
		bestBuyData2.setName("Ipad Mini speaker");
		bestBuyData2.setSalePrice(70.00);
		bestBuyList.add(bestBuyData2);

		// Compare objects with multiple records
		response = transformer.compareObjects(walmartList, bestBuyList);
		assertThat(response.getBestPrice()).isEqualTo(30.00);
	}

}
