package com.bluespurs.productfinderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductFinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductFinderApplication.class, args);
	}
}
