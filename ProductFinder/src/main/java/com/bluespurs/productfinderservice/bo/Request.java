package com.bluespurs.productfinderservice.bo;

public class Request {
private String productName;

// Parameterized Constructor
public Request(String productName){
	this.productName = productName;
}

/**
 * @return productName
 */
public String getProductName() {
	return productName;
}
}
