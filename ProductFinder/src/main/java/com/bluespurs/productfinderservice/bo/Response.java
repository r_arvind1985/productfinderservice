package com.bluespurs.productfinderservice.bo;

import com.bluespurs.productfinderservice.util.Constants;

public class Response {
	
	private String productName;
    private double bestPrice;
    private String currency;
    private String location;
    
    //Parameterized Constructor
    public Response(String productName,double bestprice, String currency, String location){
    	this.productName = productName;
    	this.bestPrice = bestprice;
    	this.currency = currency;
    	this.location = location;
    }
	/**
	 * @return productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @return bestPrice
	 */
	public double getBestPrice() {
		return bestPrice;
	}

	/**
	 * @return currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @return location
	 */
	public String getLocation() {
		return location;
	}
	
	/**
	 * Overridden method to support response logging
	 * 
	 * @return string
	 */
	@Override
	public String toString(){
		return productName  + Constants.PIPE + bestPrice + Constants.PIPE + currency + Constants.PIPE + location;
		
	}

}
