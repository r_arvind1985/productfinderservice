package com.bluespurs.productfinderservice.helper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.bluespurs.productfinderservice.util.Constants;
import com.bluespurs.productfinderservice.vo.ExternalAPIResponse;
import com.couchbase.client.deps.com.fasterxml.jackson.core.JsonParseException;
import com.couchbase.client.deps.com.fasterxml.jackson.databind.JsonMappingException;
import com.couchbase.client.deps.com.fasterxml.jackson.databind.ObjectMapper;

@Component
@ConfigurationProperties
public class RestServiceHelper {
	private static final Logger logger = LoggerFactory.getLogger(RestServiceHelper.class);
	private final RestTemplate restTemplate;

	//These URL variables can be externalized based on the development regions
	private String walmartURL = "http://api.walmartlabs.com/v1/search?apiKey=rm25tyum3p9jm9x9x7zxshfa&query={name}&facet=on&sort=relevance&order=asc&numItems=25";
	private String bestbuyURL = "https://api.bestbuy.com/v1/products(({bname})&salePrice>0&onlineAvailability=true)?apiKey=pfe9fpy68yg28hvvma49sc89&sort=salePrice.asc&pageSize=100&format=json";

	public ExternalAPIResponse externalAPIResponse;

	@Autowired
	public void setExternalAPIResponse(ExternalAPIResponse externalAPIResponse) {
		this.externalAPIResponse = externalAPIResponse;
	}

	public RestServiceHelper(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	/**
	 * 
	 * Helper method to call the external API based on the source & name parameter
	 * 
	 * @param name
	 * @param source
	 * @return externalAPIResponse
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public ExternalAPIResponse callRESTService(String name, String source) throws JsonParseException, JsonMappingException, IOException {
		Map<String, String> param = new HashMap<String, String>();
		if(source.equalsIgnoreCase(Constants.WALMART)){
			param.put(Constants.NAME, name);
		}else if(source.equalsIgnoreCase(Constants.BESTBUY)){
			param.put(Constants.BNAME, buildQuery(name));
		}
		String getResponseString = null;
		getResponseString = restTemplate.getForObject(
				source.equalsIgnoreCase(Constants.WALMART) ? walmartURL : bestbuyURL, String.class, param);
		logger.debug("Response String from " + source + ": " + getResponseString);
		externalAPIResponse = new ObjectMapper().readValue(getResponseString, ExternalAPIResponse.class);
		return externalAPIResponse;
	}
	
	/**
	 * This method is to build the search query for Best buy API as it
	 * doesn't support input parameters with space.
	 * @param name
	 * @return 
	 */
	private String buildQuery(String name){
		String returnQuery = "";
		StringTokenizer token = new StringTokenizer(name, Constants.EMPTY_STRING);
		while(token.hasMoreTokens()){
			if(returnQuery.length()>0){
				returnQuery += Constants.AMP;
			}
			returnQuery += Constants.SEARCH;
			returnQuery += token.nextToken().toString();
		}
		return returnQuery;
	}
}
