package com.bluespurs.productfinderservice.external;

import java.io.IOException;
import java.util.List;

import com.bluespurs.productfinderservice.bo.Request;
import com.bluespurs.productfinderservice.vo.BestBuyData;
import com.bluespurs.productfinderservice.vo.WalmartData;
import com.couchbase.client.deps.com.fasterxml.jackson.core.JsonParseException;
import com.couchbase.client.deps.com.fasterxml.jackson.databind.JsonMappingException;

public interface ProductFinderExternalAPI {
	
	/**
	 * This method calls the Walmart API using the API key and the 
	 * input parameter - product name 
	 * @param request
	 * @return List<WalmartData> 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public List<WalmartData> getWalmartData(Request request) throws JsonParseException, JsonMappingException, IOException;
	
	/**
	 * This method calls the Best Buy API using the API key and the 
	 * input parameter - product name 
	 * @param request
	 * @return List<BestBuyData> 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public List<BestBuyData> getBestBuyData(Request request) throws JsonParseException, JsonMappingException, IOException;
}
