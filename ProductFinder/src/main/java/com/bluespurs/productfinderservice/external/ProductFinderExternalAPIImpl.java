package com.bluespurs.productfinderservice.external;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluespurs.productfinderservice.bo.Request;
import com.bluespurs.productfinderservice.helper.RestServiceHelper;
import com.bluespurs.productfinderservice.util.Constants;
import com.bluespurs.productfinderservice.vo.BestBuyData;
import com.bluespurs.productfinderservice.vo.ExternalAPIResponse;
import com.bluespurs.productfinderservice.vo.WalmartData;
import com.couchbase.client.deps.com.fasterxml.jackson.core.JsonParseException;
import com.couchbase.client.deps.com.fasterxml.jackson.databind.JsonMappingException;
@Component
public class ProductFinderExternalAPIImpl implements ProductFinderExternalAPI {
	private static final Logger logger = LoggerFactory.getLogger(ProductFinderExternalAPIImpl.class);
	public RestServiceHelper restServiceHelper;
	public ExternalAPIResponse externalAPIResponse;
	
	@Autowired
	public void setExternalAPIResponse(ExternalAPIResponse externalAPIResponse) {
		this.externalAPIResponse = externalAPIResponse;
	}

	@Autowired
	public void setRestServiceHelper(RestServiceHelper restServiceHelper) {
		this.restServiceHelper = restServiceHelper;
	}

	@Override
	public List<WalmartData> getWalmartData(Request request) throws JsonParseException, JsonMappingException, IOException {
		logger.debug("Entering the external API call for Walmart");
		externalAPIResponse = restServiceHelper.callRESTService(request.getProductName(), Constants.WALMART);
		return externalAPIResponse.getItems();
	}

	@Override
	public List<BestBuyData> getBestBuyData(Request request) throws JsonParseException, JsonMappingException, IOException {
		logger.debug("Entering the external API call for Best Buy");
		externalAPIResponse = restServiceHelper.callRESTService(request.getProductName(),Constants.BESTBUY);
		return externalAPIResponse.getProducts();
	}

}
