package com.bluespurs.productfinderservice.util;

public class Constants {
public static final String PIPE = "|";
public static final String NAME = "name";
public static final String BNAME = "bname";
public static final String WALMART = "WALMART";
public static final String BESTBUY = "BESTBUY";
public static final String CURRENCY = "USD";
public static final String AMP = "&";
public static final String SEARCH = "search=";
public static final String EMPTY_STRING = " ";
public static final String ERRCD_400 = "400";
}
