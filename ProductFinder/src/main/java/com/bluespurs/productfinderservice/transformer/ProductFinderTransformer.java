package com.bluespurs.productfinderservice.transformer;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluespurs.productfinderservice.bo.Response;
import com.bluespurs.productfinderservice.exception.ProductNotFoundException;
import com.bluespurs.productfinderservice.util.Constants;
import com.bluespurs.productfinderservice.vo.BestBuyData;
import com.bluespurs.productfinderservice.vo.WalmartData;

@Component
public class ProductFinderTransformer {
	private static final Logger logger = LoggerFactory.getLogger(ProductFinderTransformer.class);
	public WalmartData walmartData;
	public BestBuyData bestBuyData;
	Response response = null;

	@Autowired
	public void setWalmartData(WalmartData walmartData) {
		this.walmartData = walmartData;
	}

	@Autowired
	public void setBestBuyData(BestBuyData bestBuyData) {
		this.bestBuyData = bestBuyData;
	}

	/**
	 * 
	 * This method compares the two different response objects from Walmart and
	 * Best Buy and returns the response object with lowest price
	 * 
	 * If both the list are not empty, the below steps would occur:
	 * 
	 * 1. Sort the lists based on the comparable interface implemented in the
	 * corresponding bean classes
	 * 
	 * 2. Walmart API doesn't have options to filter based on the response
	 * parameters. Call getWalmartDataByAvailability to get the WalmartData
	 * object based on the online availability
	 * 
	 * 3. For Best buy, online availability search is taken care in the query.
	 * After sorting, we will take the lower most index object from the list
	 * 
	 * 4. While testing, it was found that the Walmart API doesn't return sale
	 * price for some of its records. Hence, whenever sale price is missing,
	 * msrp would be used
	 * 
	 * 5. Based on the sale price, response object would be transformed
	 * 
	 * Else If best buy list alone is not empty, the below steps would occur
	 * 
	 * 1. Sort the Best buy list and get the lowermost record from the list
	 * 
	 * 2. Transform the response without checking for any conditions
	 * 
	 * Else If Walmart list alone is not empty, the below steps would occur
	 * 
	 * 1. Sort the Walmart list and get the lowermost record from the list by
	 * calling getWalmartDataByAvailability
	 * 
	 * 2. Whenever sale price is missing, msrp would be used
	 * 
	 * 3. Transform the response without checking for any conditions
	 * 
	 * @param walmartDataList
	 * @param bestBuyDataList
	 * @return response
	 * @throws ProductNotFoundException 
	 */
	public Response compareObjects(List<WalmartData> walmartDataList, List<BestBuyData> bestBuyDataList) throws ProductNotFoundException {
		if (walmartDataList != null && bestBuyDataList != null) {
			if (!walmartDataList.isEmpty() && !bestBuyDataList.isEmpty()) {
				Collections.sort(walmartDataList);
				Collections.sort(bestBuyDataList);
				getWalmartDataByAvailability(walmartDataList);
				bestBuyData = bestBuyDataList.get(0);
				if (walmartData.getSalePrice() == 0.0) {
					walmartData.setSalePrice(walmartData.getMsrp());
				}
				if (walmartData.getSalePrice() < bestBuyData.getSalePrice()) {
					response = transformResponse(walmartData, null);
				} else {
					response = transformResponse(null, bestBuyData);
				}
			} else if (walmartDataList.isEmpty() && !bestBuyDataList.isEmpty()) {
				Collections.sort(bestBuyDataList);
				bestBuyData = bestBuyDataList.get(0);
				response = transformResponse(null, bestBuyData);
			} else if (!walmartDataList.isEmpty() && bestBuyDataList.isEmpty()) {
				Collections.sort(walmartDataList);
				getWalmartDataByAvailability(walmartDataList);
				if (walmartData.getSalePrice() == 0.0) {
					walmartData.setSalePrice(walmartData.getMsrp());
				}
				response = transformResponse(walmartData, null);
			}
			// toString in Response is overridden for logging purposes
			logger.info("Final response string : " + response.toString());
		}else{
			throw new ProductNotFoundException();
		}
		return response;

	}

	/**
	 * 
	 * Transformer method to build the response object based on the source data
	 * 
	 * @param walmartResp
	 * @param bestBuyResp
	 * @return response
	 */
	private Response transformResponse(WalmartData walmartResp, BestBuyData bestBuyResp) {
		if (walmartResp != null) {
			response = new Response(walmartResp.getName(), walmartResp.getSalePrice(), Constants.CURRENCY,
					Constants.WALMART);
		} else if (bestBuyResp != null) {
			response = new Response(bestBuyResp.getName(), bestBuyResp.getSalePrice(), Constants.CURRENCY,
					Constants.BESTBUY);
		}
		return response;

	}

	/**
	 * 
	 * Walmart API doesn't have options to query based on the response
	 * parameters. This method is to retrieve the lowest walmartData object with
	 * availableOnline set to true
	 * 
	 * @param walmartDataList
	 * @return
	 */
	private WalmartData getWalmartDataByAvailability(List<WalmartData> walmartDataList) {
		for (int i = 0; i < walmartDataList.size(); i++) {
			walmartData = walmartDataList.get(i);
			if (walmartData.isAvailableOnline()) {
				break;
			} else {
				continue;
			}
		}
		return walmartData;

	}
}
