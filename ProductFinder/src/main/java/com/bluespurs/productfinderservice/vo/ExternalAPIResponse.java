package com.bluespurs.productfinderservice.vo;

import java.util.List;

import org.springframework.stereotype.Component;

import com.couchbase.client.deps.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
@Component
public class ExternalAPIResponse {
	private List<WalmartData> items;
	private List<BestBuyData> products;
	
	public List<BestBuyData> getProducts() {
		return products;
	}

	public void setProducts(List<BestBuyData> products) {
		this.products = products;
	}

	public List<WalmartData> getItems() {
		return items;
	}

	public void setItems(List<WalmartData> items) {
		this.items = items;
	}

	private String query;
	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	private String sort;

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

}
