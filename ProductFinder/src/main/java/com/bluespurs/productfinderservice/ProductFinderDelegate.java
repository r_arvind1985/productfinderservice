package com.bluespurs.productfinderservice;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.bluespurs.productfinderservice.bo.Request;
import com.bluespurs.productfinderservice.bo.Response;
import com.bluespurs.productfinderservice.exception.ProductNotFoundException;
import com.bluespurs.productfinderservice.external.ProductFinderExternalAPI;
import com.bluespurs.productfinderservice.transformer.ProductFinderTransformer;
import com.bluespurs.productfinderservice.util.Constants;
import com.bluespurs.productfinderservice.vo.BestBuyData;
import com.bluespurs.productfinderservice.vo.WalmartData;

@Component
public class ProductFinderDelegate {
	private static final Logger logger = LoggerFactory.getLogger(ProductFinderDelegate.class);
	RestTemplate restTemplate = new RestTemplate();
	public ProductFinderExternalAPI productFinderExternalAPI;
	public ProductFinderTransformer productFinderTransformer;

	@Autowired
	public void setProductFinderExternalAPI(ProductFinderExternalAPI productFinderExternalAPI) {
		this.productFinderExternalAPI = productFinderExternalAPI;
	}

	@Autowired
	public void setProductFinderTransformer(ProductFinderTransformer productFinderTransformer) {
		this.productFinderTransformer = productFinderTransformer;
	}

	/**
	 * This method in the delegate class will call the two external API
	 * interfaces to get the data for comparison
	 * 
	 * @param request
	 * @return response
	 * @throws ProductNotFoundException
	 */
	public Response getProductDetails(Request request) throws ProductNotFoundException {
		logger.info("Inside the delegate class with input parameter " + request.getProductName());
		List<WalmartData> walmartDataList = new ArrayList<WalmartData>();
		List<BestBuyData> bestBuyDataList = new ArrayList<BestBuyData>();
		try {
			// Walmart API data
			walmartDataList = productFinderExternalAPI.getWalmartData(request);
			// Best Buy API data
			bestBuyDataList = productFinderExternalAPI.getBestBuyData(request);

		} catch (Exception e) {
			logger.error("Exception occured : " + e.getLocalizedMessage());
			if (e.getMessage().contains(Constants.ERRCD_400)) {
				throw new BadRequestException(e.getMessage());
			} else {
				throw new ProductNotFoundException();
			}
		}
		return productFinderTransformer.compareObjects(walmartDataList, bestBuyDataList);
	}
}
