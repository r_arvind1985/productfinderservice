package com.bluespurs.productfinderservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bluespurs.productfinderservice.bo.Request;
import com.bluespurs.productfinderservice.bo.Response;
import com.bluespurs.productfinderservice.exception.ProductNotFoundException;

@RestController
public class ProductFinderEndPoint {
	public ProductFinderDelegate productFinderDelegate;

	@Autowired
	public void setProductFinderDelegate(ProductFinderDelegate productFinderDelegate) {
		this.productFinderDelegate = productFinderDelegate;
	}

	/**
	 * Entry point for the request path /product/search
	 * 
	 * @param productName
	 * @return response
	 */
	@RequestMapping(value = "/product/search", method = RequestMethod.GET)
	public Response productSearch(@RequestParam(value = "name") String productName) throws ProductNotFoundException {
		return productFinderDelegate.getProductDetails(new Request(productName));
	}

}
